package com.classpath.inventorymicroservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
@Slf4j
public class InventoryController {

    private static Integer counter = 1000;
    @GetMapping
    public Integer getCount(){
        return counter;
    }

    @PostMapping
    public Integer updateQty(){
        log.info(" Inside the updateQty method of Inventory microservice");
        return -- counter;
    }
}
